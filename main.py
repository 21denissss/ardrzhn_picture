from dataclasses import dataclass

import cv2
import numpy as np


@dataclass
class Image:
    img_input: np.ndarray
    img_output: np.ndarray


class ImagePreparingUtils:
    @classmethod
    def _load(cls, file_name: str) -> Image:
        """
        Открытие изображения

        :param file_name: Расположение изображения
        :return: Изображение
        :raise FileExistsError: Проблема в открытии изображения
        """
        img = cv2.imread(file_name)
        if type(img) is not np.ndarray:
            raise FileExistsError
        return Image(img_input=img, img_output=img)

    @classmethod
    def _prepare(cls, img: Image) -> Image:
        """
        Подготовка изображения

        :param img: Изображение
        :return: Подготовленное изображение
        """
        img_input = img.img_input.copy()

        # Цвет в который перекрасим
        gray = cv2.cvtColor(img_input, cv2.COLOR_BGR2GRAY)

        # Изменяем цвет(пороговое значение)
        ret, thresh = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY)

        # Увеличиваем
        img_output = cv2.erode(thresh, np.ones((3, 3), np.uint8), iterations=1)
        return Image(img_input=img.img_input, img_output=img_output)

    @classmethod
    def _scaling(cls, img: Image, out_size: int = 28) -> list[tuple[int, int, np.ndarray]]:
        """
        Изменяем размер букв

        :param img: Изображение
        :param out_size: Размер выходного изображения
        :return: Список букв
        """
        img_input = img.img_input.copy()

        # Цвет в который перекрасим
        gray = cv2.cvtColor(img_input, cv2.COLOR_BGR2GRAY)

        # Получаем контуры буквы
        contours, hierarchy = cv2.findContours(img.img_output, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
        letters = []
        for idx, contour in enumerate(contours):
            # Получаем координаты, высоту и ширину контура
            x, y, w, h = cv2.boundingRect(contour)

            # Изменяем размер букв(делаем их в форме квадрата)
            if hierarchy[0][idx][3] == 0:
                letter_crop = gray[y:y + h, x:x + w]
                size_max = max(w, h)
                letter_square = 255 * np.ones(shape=[size_max, size_max], dtype=np.uint8)
                if w > h:
                    y_pos = size_max // 2 - h // 2
                    letter_square[y_pos:y_pos + h, 0:w] = letter_crop
                elif w < h:
                    x_pos = size_max // 2 - w // 2
                    letter_square[0:h, x_pos:x_pos + w] = letter_crop
                else:
                    letter_square = letter_crop
                letters.append((x, w, cv2.resize(letter_square, (out_size, out_size), interpolation=cv2.INTER_AREA)))

        # Сортируем по оси X
        letters.sort(key=lambda x: x[0], reverse=False)
        return letters

    @classmethod
    def execute(cls, file_name: str) -> list[tuple[int, int, np.ndarray]]:
        """
        Подготовка изображения для дальнейшей обработки

        :param file_name: Расположение изображения
        :return: Список букв
        :raise FileExistsError: Проблема в открытии изображения
        """
        try:
            img = cls._load(file_name)
            img = cls._prepare(img)
            letters = cls._scaling(img)
        except FileExistsError:
            raise FileExistsError(f'Не смог открыть изображение: {file_name}')

        return letters

import keras
import idx2numpy

# class ImageModelTrainingService:
#     def execute(self):
#         emnist_path = '/home/Documents/TestApps/keras/emnist/'
#         X_train = idx2numpy.convert_from_file(emnist_path + 'emnist-byclass-train-images-idx3-ubyte')
#         y_train = idx2numpy.convert_from_file(emnist_path + 'emnist-byclass-train-labels-idx1-ubyte')
#
#         X_test = idx2numpy.convert_from_file(emnist_path + 'emnist-byclass-test-images-idx3-ubyte')
#         y_test = idx2numpy.convert_from_file(emnist_path + 'emnist-byclass-test-labels-idx1-ubyte')
#
#         X_train = np.reshape(X_train, (X_train.shape[0], 28, 28, 1))
#         X_test = np.reshape(X_test, (X_test.shape[0], 28, 28, 1))
#
#         print(X_train.shape, y_train.shape, X_test.shape, y_test.shape, len(emnist_labels))
#
#         k = 10
#         X_train = X_train[:X_train.shape[0] // k]
#         y_train = y_train[:y_train.shape[0] // k]
#         X_test = X_test[:X_test.shape[0] // k]
#         y_test = y_test[:y_test.shape[0] // k]
#
#         # Normalize
#         X_train = X_train.astype(np.float32)
#         X_train /= 255.0
#         X_test = X_test.astype(np.float32)
#         X_test /= 255.0
#
#         x_train_cat = keras.utils.to_categorical(y_train, len(emnist_labels))
#         y_test_cat = keras.utils.to_categorical(y_test, len(emnist_labels))


if __name__ == '__main__':
    letters = ImagePreparingUtils.execute("test.png")
